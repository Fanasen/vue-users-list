export class User {
    user;

    constructor(user) {
        this.user = user;
        this.user.age = this.calculateAge();
        this.user.status = user.active ? 'ACTIVE' : 'INACTIVE';
        this.user.birthDate = this.birthDate();
    }

    calculateAge() {
        var dayOfBirth = this.user.dob;
        var dob = new Date(dayOfBirth);
        var today = new Date();
        var age = (today.getUTCFullYear() - dob.getUTCFullYear());
        var month = today.getUTCMonth() - dob.getUTCMonth();
        var day = today.getUTCDate() - dob.getUTCDate();
        var hour = today.getUTCHours() - dob.getUTCHours();
        if( month < 0 || (month === 0 && day < 0) || (month === 0 && day === 0 && hour < 0)){
            age--;
        }
        return age;
    }

    birthDate(){
        var dob = new Date(this.user.dob);
        return dob.getUTCFullYear() + '.' + dob.getUTCMonth() + '.' + dob.getUTCDate();
    }
}